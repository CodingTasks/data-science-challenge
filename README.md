# Data Science Challenge

## Introduction

In this challenge, we are looking at your ability to train a model on a specific task and provide this model to others via an api.

The challenge should train a model with a data set of traffic lights.

- train a model on a data set of traffic lights
- the model should return the likelihood of an image being a traffic light
- the model should also return the likelihood of the traffic light being in a specific state (red, yellow, green, off)
- this model should be made accessible to others via an api

Example:
```
$ curl http://localhost/api?url=https://s0.geograph.org.uk/geophotos/05/68/96/5689659_cfb578fa_original.jpg
{
  trafficLightProbability: 0.8125,
  redTrafficLightProbability: 0.7125,
  yellowTrafficLightProbability: 0.0125,
  greenTrafficLightProbability: 0.1125,
  offTrafficLightProbability: 0.0525
}
```

## Api Design

- you are free to design the api according to your specifications, we want to see how likely it is that the image contains a traffic light and how likely it is to be in a certain state
- To send the images to the api, you can either upload them to the api or give the api an image url

## Tools

- you are free to use whatever tool best suits your workflow
- if you need computing capacity we can provide you with an aws ec2 instance of your choice
- if you need anything else, please get in touch with us
- you are free to host the rest api however you like, it should be accessible from public internet

## Result

- in the end we want to have an api where we can upload images and get results back
- we expect documentation on how to use your api
- in our second interview we expect to get a walkthrough of your workflow
- in the second interview we also focus on how well you can explain how you worked, why you did certain things and what the trade-offs were
- trade-offs are fine as long as there are reasons for them
- we expect to get a good explanation at the end
- document the trade-offs and limitations. When is your api working as expected, when might we run into problems? e.g. multiple traffic lights, big images, small traffic light in panorama pictures

## Questions

- if you are unsure about certain aspects, please get in touch with us

## Time Constraints

- the task should be done within one week

## Resources

- There is a data set available at https://hci.iwr.uni-heidelberg.de/node/6132